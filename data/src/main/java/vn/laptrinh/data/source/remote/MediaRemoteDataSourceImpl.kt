package vn.laptrinh.data.source.remote

import retrofit2.Retrofit
import vn.laptrinh.domain.model.Media
import vn.laptrinh.domain.source.remote.MediaRemoteDataSource
import javax.inject.Inject

class MediaRemoteDataSourceImpl @Inject constructor(
    private val retrofit: Retrofit
) : MediaRemoteDataSource {
    override suspend fun getMedia(): Media? {
        return try {
            retrofit.create(MediaWebService::class.java).getMedia()
        } catch (e: Exception) {
            null
        }
    }
}