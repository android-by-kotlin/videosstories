package vn.laptrinh.data.source.remote

import retrofit2.http.GET
import vn.laptrinh.domain.model.Media

interface MediaWebService {

    @GET("/api/json-storage/bin/edfefba")
    suspend fun getMedia(): Media?

}