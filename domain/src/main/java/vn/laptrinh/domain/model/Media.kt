package vn.laptrinh.domain.model

data class Media(
    val videos: List<Video>,
    val stories: List<Story>
)