package vn.laptrinh.domain.model

data class Sport(
    val id: Int,
    val name: String
)