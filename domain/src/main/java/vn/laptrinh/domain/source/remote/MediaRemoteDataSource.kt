package vn.laptrinh.domain.source.remote

import vn.laptrinh.domain.model.Media

interface MediaRemoteDataSource {

    suspend fun getMedia(): Media?
}