package vn.laptrinh.domain.usecase

import vn.laptrinh.domain.source.remote.MediaRemoteDataSource
import javax.inject.Inject

class GetMediaUseCase @Inject constructor(
    private val mediaRemoteDataSource: MediaRemoteDataSource
) {

    suspend operator fun invoke() = mediaRemoteDataSource.getMedia()
}