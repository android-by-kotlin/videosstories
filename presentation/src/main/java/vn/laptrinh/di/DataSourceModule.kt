package vn.laptrinh.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import vn.laptrinh.data.source.remote.MediaRemoteDataSourceImpl
import vn.laptrinh.domain.source.remote.MediaRemoteDataSource

@InstallIn(SingletonComponent::class)
@Module
abstract class DataSourceModule {

    @Binds
    abstract fun provideMediaRemoteDataSource(
        dataSourceImpl: MediaRemoteDataSourceImpl
    ): MediaRemoteDataSource
}
