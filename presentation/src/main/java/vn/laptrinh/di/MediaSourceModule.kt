package vn.laptrinh.di

import android.content.Context
import androidx.annotation.OptIn
import androidx.media3.common.util.UnstableApi
import androidx.media3.database.StandaloneDatabaseProvider
import androidx.media3.datasource.DefaultDataSource
import androidx.media3.datasource.DefaultHttpDataSource
import androidx.media3.datasource.FileDataSource
import androidx.media3.datasource.cache.CacheDataSink
import androidx.media3.datasource.cache.CacheDataSource
import androidx.media3.datasource.cache.NoOpCacheEvictor
import androidx.media3.datasource.cache.SimpleCache
import androidx.media3.exoplayer.source.DefaultMediaSourceFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import java.io.File
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class MediaSourceModule {

    @OptIn(UnstableApi::class)
    @Provides
    @Singleton
    fun provideDefaultMediaSourceFactory(
        context: Context
    ): DefaultMediaSourceFactory {
        val directory = File(
            context.cacheDir,
            "video_cache"
        )
        val videoCache = SimpleCache(
            directory,
            NoOpCacheEvictor(),
            StandaloneDatabaseProvider(context)
        )
        val cacheDataSink = CacheDataSink.Factory().setCache(videoCache)
        val upstreamDataSourceFactory = DefaultDataSource.Factory(
            context,
            DefaultHttpDataSource.Factory()
        )
        val cacheDataSource = CacheDataSource.Factory()
            .setCache(videoCache)
            .setCacheWriteDataSinkFactory(cacheDataSink)
            .setCacheReadDataSourceFactory(FileDataSource.Factory())
            .setUpstreamDataSourceFactory(upstreamDataSourceFactory)
            .setFlags(CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR)

        return DefaultMediaSourceFactory(cacheDataSource)
    }
}