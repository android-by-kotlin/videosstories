package vn.laptrinh.presentation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import dagger.hilt.android.AndroidEntryPoint
import vn.laptrinh.football.presentation.component.ErrorComponent
import vn.laptrinh.presentation.component.HomeComponent
import vn.laptrinh.presentation.component.LoaderComponent
import vn.laptrinh.presentation.ui.model.HomeUIModel
import vn.laptrinh.presentation.ui.state.HomeUIState
import vn.laptrinh.presentation.ui.theme.VideosStoriesTheme

@AndroidEntryPoint
class HomeActivity : ComponentActivity() {

    private val viewModel: HomeViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        viewModel.initUIState()
        setContent {
            VideosStoriesTheme {
                Scaffold(modifier = Modifier.fillMaxSize()) {
                    when (val uiState = viewModel.uiState.collectAsStateWithLifecycle(lifecycleOwner = LocalLifecycleOwner.current).value) {
                        HomeUIState.Loading -> LoaderComponent(modifier = Modifier.padding(it))
                        is HomeUIState.Error -> ErrorComponent(
                            uiModel = uiState.uiModel,
                            onButtonClick = { viewModel.initUIState() }
                        )
                        is HomeUIState.Done -> ScreenContainer(uiModel = uiState.uiModel)
                    }
                }
            }
        }
    }
}

@Composable
fun ScreenContainer(
    uiModel: HomeUIModel
) {
    val activity = LocalContext.current as Activity
    HomeComponent(
        uiModel = uiModel,
        onVideoClick = {
            val intent = Intent(activity, PlayVideoActivity::class.java)
            intent.putExtra(VIDEO_URL, it)
            activity.startActivity(intent)
        }
    )
}