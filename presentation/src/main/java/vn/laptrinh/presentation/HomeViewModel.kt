package vn.laptrinh.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import vn.laptrinh.domain.usecase.GetMediaUseCase
import vn.laptrinh.presentation.ui.model.ErrorUIModel
import vn.laptrinh.presentation.ui.model.HomeUIModel
import vn.laptrinh.presentation.ui.model.TextUIModel
import vn.laptrinh.presentation.ui.model.VideoUIModel
import vn.laptrinh.presentation.ui.state.HomeUIState
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getMediaUseCase: GetMediaUseCase
) : ViewModel() {

    private val _uiState = MutableStateFlow<HomeUIState>(HomeUIState.Loading)
    val uiState: StateFlow<HomeUIState> = _uiState.asStateFlow()

    fun initUIState() {
        _uiState.update { HomeUIState.Loading }
        viewModelScope.launch {
            _uiState.update {
                when (val media = getMediaUseCase()) {
                    null -> HomeUIState.Error(
                        uiModel = ErrorUIModel(
                            errorMessage = TextUIModel.DynamicString("Load videos error")
                        )
                    )
                    else -> HomeUIState.Done(
                        uiModel = HomeUIModel(
                            videos = media.videos.map { video ->
                                VideoUIModel(
                                    imageUrl = video.thumb,
                                    url = video.url
                                )
                            }
                        )
                    )
                }
            }
        }
    }
}