package vn.laptrinh.presentation

import PlayVideoComponent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import dagger.hilt.android.AndroidEntryPoint
import vn.laptrinh.presentation.ui.model.PlayVideoUIModel
import vn.laptrinh.presentation.ui.theme.ColorByTheme
import vn.laptrinh.presentation.ui.theme.VideosStoriesTheme

@AndroidEntryPoint
class PlayVideoActivity : ComponentActivity() {

    private val viewModel: PlayVideoViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        val bundle = intent.extras
        val url = bundle?.getString(VIDEO_URL).orEmpty()
        // Not need to recreate the player and play again when change the config (rotation, ect)
        if (viewModel.videoPlayer == null) {
            viewModel.initUIStateAndPlayVideo(
                context = this,
                url = url
            )
        }
        setContent {
            VideosStoriesTheme {
                val uiModel = viewModel.uiState.collectAsStateWithLifecycle(lifecycleOwner = LocalLifecycleOwner.current).value
                uiModel?.let {
                    ScreenContainer(
                        modifier = Modifier.fillMaxSize(),
                        viewModel = viewModel,
                        uiModel = uiModel
                    )
                }
            }
        }
    }
}

@Composable
fun ScreenContainer(
    modifier: Modifier = Modifier,
    viewModel: PlayVideoViewModel,
    uiModel: PlayVideoUIModel
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .background(
                color = ColorByTheme(
                    light = Color.White,
                    dark = Color.Black
                )()
            ),
        verticalArrangement = Arrangement.Center
    ) {
        PlayVideoComponent(
            uiModel = uiModel,
            onRetryClick = { viewModel.retryPlayVideo() }
        )
    }
}