package vn.laptrinh.presentation

import android.content.Context
import androidx.annotation.OptIn
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.media3.common.MediaItem
import androidx.media3.common.PlaybackException
import androidx.media3.common.Player
import androidx.media3.common.util.UnstableApi
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.source.DefaultMediaSourceFactory
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import vn.laptrinh.presentation.ui.model.ErrorUIModel
import vn.laptrinh.presentation.ui.model.PlayVideoUIModel
import vn.laptrinh.presentation.ui.model.TextUIModel
import javax.inject.Inject

@HiltViewModel
class PlayVideoViewModel @Inject constructor(
    private val defaultMediaSourceFactory: DefaultMediaSourceFactory
) : ViewModel() {

    private val _uiState = MutableStateFlow<PlayVideoUIModel?>(null)
    val uiState: StateFlow<PlayVideoUIModel?> = _uiState.asStateFlow()

    var videoPlayer: ExoPlayer? = null

    @OptIn(UnstableApi::class)
    fun initUIStateAndPlayVideo(
        context: Context,
        url: String
    ) {
        viewModelScope.launch {
            val playerListener = object : Player.Listener {

                override fun onPlayerError(error: PlaybackException) {
                    super.onPlayerError(error)

                    println("ExoPlayerVideo: onPlayerError: $error")

                    _uiState.update {
                        PlayVideoUIModel.Error(
                            errorUIModel = ErrorUIModel(
                                errorMessage = TextUIModel.DynamicString("Load video error")
                            )
                        )
                    }
                }

                override fun onIsPlayingChanged(isPlaying: Boolean) {
                    super.onIsPlayingChanged(isPlaying)

                    println("ExoPlayerVideo: onIsPlayingChanged: $isPlaying")

                    if (isPlaying) {
                        if (_uiState.value is PlayVideoUIModel.Play) return
                        _uiState.update { PlayVideoUIModel.Play(videoPlayer) }
                    }
                }
            }

            _uiState.update { PlayVideoUIModel.Loading }

            videoPlayer = ExoPlayer.Builder(context).build().apply {
                addListener(playerListener)
                playWhenReady = true
                val mediaSource = defaultMediaSourceFactory.createMediaSource(
                    MediaItem.fromUri(url)
                )
                setMediaSource(mediaSource)
                prepare()
            }
        }
    }

    fun retryPlayVideo() {
        _uiState.update { PlayVideoUIModel.Loading }
        videoPlayer?.prepare()
    }

    override fun onCleared() {
        super.onCleared()
        videoPlayer?.release()
        videoPlayer = null
    }
}