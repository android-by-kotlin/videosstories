package vn.laptrinh.presentation.annotation

import android.content.res.Configuration
import androidx.compose.ui.tooling.preview.Preview

@Preview(name = "Small font", group = "Font scales", fontScale = 0.5f, showBackground = true)
@Preview(name = "Normal font", group = "Font scales", showBackground = true)
@Preview(name = "Large font", group = "Font scales", fontScale = 1.5f, showBackground = true)
annotation class FontScalePreviews

@Preview(
    name = "Dark mode",
    group = "UI mode",
    uiMode = Configuration.UI_MODE_NIGHT_YES or Configuration.UI_MODE_TYPE_NORMAL,
    showBackground = true
)
@Preview(
    name = "Light mode",
    group = "UI mode",
    uiMode = Configuration.UI_MODE_NIGHT_NO or Configuration.UI_MODE_TYPE_NORMAL,
    showBackground = true
)
annotation class DarkLightPreviews

@Preview(
    name = "phone",
    group = "devices",
    device = "spec:shape=Normal,width=360,height=640,unit=dp,dpi=480",
    showSystemUi = true
)
@Preview(
    name = "landscape",
    group = "devices",
    device = "spec:shape=Normal,width=640,height=360,unit=dp,dpi=480",
    showSystemUi = true
)
@Preview(
    name = "foldable",
    group = "devices",
    device = "spec:shape=Normal,width=673,height=841,unit=dp,dpi=480",
    showSystemUi = true
)
@Preview(
    name = "tablet",
    group = "devices",
    device = "spec:shape=Normal,width=1280,height=800,unit=dp,dpi=480",
    showSystemUi = true
)
annotation class DevicePreviews

@FontScalePreviews
@DevicePreviews
@DarkLightPreviews
annotation class CombinedPreviews