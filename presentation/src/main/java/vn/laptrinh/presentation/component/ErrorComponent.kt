package vn.laptrinh.football.presentation.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import vn.laptrinh.presentation.R
import vn.laptrinh.presentation.ui.model.ErrorUIModel
import vn.laptrinh.presentation.ui.theme.ColorByTheme

@Composable
fun ErrorComponent(
    modifier: Modifier = Modifier,
    uiModel: ErrorUIModel,
    onButtonClick: () -> Unit
) {
    Column(
        modifier = modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            modifier = Modifier.size(200.dp),
            painter = painterResource(id = R.drawable.ic_broken_image),
            contentDescription = ""
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = uiModel.errorMessage.asString(),
            color = ColorByTheme(
                light = Color.Black,
                dark = Color.White
            )(),
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold
        )
        Spacer(modifier = Modifier.height(16.dp))
        Button(
            modifier = Modifier.padding(16.dp),
            onClick = onButtonClick,
            colors = ButtonDefaults.textButtonColors(
                contentColor = ColorByTheme(
                    light = Color.White,
                    dark = Color.Black
                )(),
                containerColor = ColorByTheme(
                    light = Color(0xFF00008B),
                    dark = Color(0xFF45B6FE)
                )()
            )
        ) {
            Text(
                text = "Retry",
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold
            )
        }
    }
}