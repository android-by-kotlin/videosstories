package vn.laptrinh.presentation.component

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import vn.laptrinh.presentation.ui.model.HomeUIModel

@Composable
fun HomeComponent(
    modifier: Modifier = Modifier,
    uiModel: HomeUIModel,
    onVideoClick: (url: String) -> Unit
) {
    LazyColumn(
        modifier = modifier
            .padding(16.dp)
            .fillMaxSize()
    ) {
        items(uiModel.videos) {
            Spacer(modifier = Modifier.height(24.dp))
            VideoComponent(
                uiModel = it,
                onClick = { onVideoClick(it.url) }
            )
        }
    }
}