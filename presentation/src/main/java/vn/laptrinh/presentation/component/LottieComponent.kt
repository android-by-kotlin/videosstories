package vn.laptrinh.presentation.component

import androidx.annotation.RawRes
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.rememberLottieComposition

@Composable
fun LottieComponent(
    modifier: Modifier = Modifier,
    @RawRes lottieResId: Int
) {
    val composition by rememberLottieComposition(
        LottieCompositionSpec.RawRes(lottieResId)
    )
    LottieAnimation(
        modifier = modifier,
        contentScale = ContentScale.Fit,
        iterations = LottieConstants.IterateForever,
        composition = composition,
    )
}