import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.compose.LocalLifecycleOwner
import androidx.media3.ui.AspectRatioFrameLayout
import androidx.media3.ui.PlayerView
import vn.laptrinh.football.presentation.component.ErrorComponent
import vn.laptrinh.presentation.component.VideoLoaderComponent
import vn.laptrinh.presentation.ui.model.PlayVideoUIModel

@Composable
@androidx.annotation.OptIn(androidx.media3.common.util.UnstableApi::class)
fun PlayVideoComponent(
    modifier: Modifier = Modifier,
    uiModel: PlayVideoUIModel,
    onRetryClick: () -> Unit
) {
    var lifecycle by remember { mutableStateOf(Lifecycle.Event.ON_CREATE) }
    val lifecycleOwner = LocalLifecycleOwner.current
    DisposableEffect(lifecycleOwner) {
        val observer = LifecycleEventObserver { _, event -> lifecycle = event }
        lifecycleOwner.lifecycle.addObserver(observer)
        onDispose { lifecycleOwner.lifecycle.removeObserver(observer) }
    }
    when (uiModel) {
        PlayVideoUIModel.Loading -> VideoLoaderComponent()
        is PlayVideoUIModel.Error -> ErrorComponent(
            uiModel = uiModel.errorUIModel,
            onButtonClick = onRetryClick
        )
        is PlayVideoUIModel.Play -> AndroidView(
            modifier = modifier
                .fillMaxWidth()
                .height(IntrinsicSize.Max),
            factory = { context ->
                PlayerView(context).also {
                    it.player = uiModel.videoPlayer
                    it.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM
                }
            },
            update = {
                it.player = uiModel.videoPlayer
                when (lifecycle) {
                    Lifecycle.Event.ON_PAUSE -> {
                        it.onPause()
                        it.player?.pause()
                        it.player?.playWhenReady = false
                    }
                    Lifecycle.Event.ON_RESUME -> {
                        it.onResume()
                        it.player?.playWhenReady = true
                    }
                    else -> Unit
                }
            }
        )
    }
}