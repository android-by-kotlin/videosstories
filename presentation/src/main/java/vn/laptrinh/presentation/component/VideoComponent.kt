package vn.laptrinh.presentation.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import vn.laptrinh.presentation.R
import vn.laptrinh.presentation.ui.model.VideoUIModel
import vn.laptrinh.presentation.ui.theme.ColorByTheme

@Composable
fun VideoComponent(
    modifier: Modifier = Modifier,
    uiModel: VideoUIModel,
    onClick: () -> Unit
) {
    Card(
        modifier = modifier.fillMaxWidth(),
        shape = RoundedCornerShape(16.dp),
        elevation = CardDefaults.cardElevation(8.dp),
        colors = CardDefaults.cardColors(
            containerColor = ColorByTheme(
                light = Color.White,
                dark = Color.DarkGray
            )()
        ),
        onClick = onClick
    ) {
        Box(modifier = modifier) {
            AsyncImage(
                model = uiModel.imageUrl,
                contentDescription = null,
                contentScale = ContentScale.Crop,
                error = painterResource(id = R.drawable.ic_broken_image),
                placeholder = painterResource(id = R.drawable.ic_loading_image)
            )
            Image(
                modifier = modifier.align(Alignment.Center),
                painter = painterResource(id = R.drawable.ic_play),
                contentDescription = null
            )
        }

    }
}