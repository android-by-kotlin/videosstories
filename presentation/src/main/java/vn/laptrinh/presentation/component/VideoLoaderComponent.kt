package vn.laptrinh.presentation.component

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import vn.laptrinh.presentation.R
import vn.laptrinh.presentation.annotation.CombinedPreviews

@Composable
fun VideoLoaderComponent(
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        LottieComponent(
            modifier = modifier.width(150.dp),
            lottieResId = R.raw.ic_loader
        )
    }
}

@Composable
@CombinedPreviews
fun PreviewVideoLoaderComponent() {
    VideoLoaderComponent()
}