package vn.laptrinh.presentation.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class HomeUIModel(
    val videos: List<VideoUIModel>
)