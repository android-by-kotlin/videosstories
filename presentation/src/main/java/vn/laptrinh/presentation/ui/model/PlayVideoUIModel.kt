package vn.laptrinh.presentation.ui.model

import androidx.compose.runtime.Immutable
import androidx.media3.common.Player

@Immutable
sealed class PlayVideoUIModel {

    data object Loading : PlayVideoUIModel()

    data class Error(
        val errorUIModel: ErrorUIModel
    ) : PlayVideoUIModel()

    data class Play(
        val videoPlayer: Player?
    ) : PlayVideoUIModel()
}