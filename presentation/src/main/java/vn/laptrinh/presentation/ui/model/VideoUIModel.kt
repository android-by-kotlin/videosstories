package vn.laptrinh.presentation.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class VideoUIModel(
    val imageUrl: String,
    val url: String
)
