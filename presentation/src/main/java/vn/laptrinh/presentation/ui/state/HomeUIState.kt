package vn.laptrinh.presentation.ui.state

import androidx.compose.runtime.Immutable
import vn.laptrinh.presentation.ui.model.ErrorUIModel
import vn.laptrinh.presentation.ui.model.HomeUIModel

@Immutable
sealed class HomeUIState {

    @Immutable
    data object Loading : HomeUIState()

    @Immutable
    data class Error(
        val uiModel: ErrorUIModel
    ) : HomeUIState()

    @Immutable
    data class Done(
        val uiModel: HomeUIModel
    ) : HomeUIState()
}