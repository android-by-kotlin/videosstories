package vn.laptrinh.presentation.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.ui.graphics.Color

@Immutable
data class ColorByTheme(
    val light: Color,
    val dark: Color
) {
    constructor(
        color: Color
    ) : this(
        light = color,
        dark = color
    )

    @Composable
    operator fun invoke(): Color {
        return if (isSystemInDarkTheme()) dark else light
    }
}